# -*- coding: utf-8 -*-
'''
For more details than those found in the documentation, code comments and
references, please let me know at:
    nicolas _dot_ desmars _at_ protonmail _dot_ com

License
----------
Copyright 2024 Nicolas Desmars

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

from numpy import pi, linspace, exp, sqrt, hypot, meshgrid, nonzero, real, \
    hstack, fft, tanh, cos, random, cosh, sinh
import sys


class OceanSurface():
    '''
    Class that provides methods to generate a wave field an external spectrum.

    Reference
    ----------
    The ocean surface is modeled according to the Improved Choppy Wave Model:

    Guérin, C.-A., Desmars, N., Grilli, S. T., Ducrozet, G., Perignon, Y. &
    Ferrant, P. (2019) An improved Lagrangian model for the time evolution of
    nonlinear surface waves. Journal of Fluid Mechanics 876, 527–552.
    '''

    def __init__(self, **kwargs):
        '''
        Constructor.

        kwargs
        ----------
        Nk :: int :: Number of wave components along each direction.
        phase_seed :: int :: Seed for the initial phase distribution.
        '''
        self.Nk = kwargs.get('Nk', 256)
        self.phase_seed = kwargs.get('phase_seed', 0)

    def get_modal_amplitudes_external_input(self, Sk, k_vec, **kwargs):
        '''
        Method that calculates the complex modal amplitudes of the directional
        spectrum and nonlinear quantities from an external input.

        Inputs
        ----------
        Sk :: 1darray :: Directional free surface elevation variance spectrum
                         as a function of the wavenumber.
        k_vec :: 1darray :: Regular vector of wavenumbers that fits to both x-
                            and y-directions. The wavenumber discretization of
                            the spectrum has to stay constant in time.

        kwargs
        ----------
        mu :: float :: Non-dimensional water depth, based on the longest wave
                       mu = 2*pi/L*depth (= dk*depth).
        '''
        # check that the number of wave components fits the wave model
        if Sk.shape != (self.Nk, self.Nk) or len(k_vec) != self.Nk:
            sys.exit('Please use a wave spectrum Sk and a wavenumber vector '
                     'k_vec that have appropriate number of wave components '
                     'Nk.')
        # calculate some useful grid parameters
        if not hasattr(self, 'k') or self.dk != k_vec[1]-k_vec[0]:
            # get the non-dimensional water depth defined based on the longest
            # wave mu = 2*pi/L*depth = depth*dk
            self.mu = kwargs.get('mu', 0.)
            # wavenumber step
            self.dk = k_vec[1]-k_vec[0]
            # space extent of computational domain in x- and y-directions
            self.L = 2*pi/self.dk
            # wavenumber grids in x- and y-directions
            self.kx, self.ky = meshgrid(k_vec, k_vec, indexing='ij')
            # grid of absolute value of wavenumbers
            self.k = hypot(self.kx, self.ky)
            # water depth
            depth = self.mu/self.dk
            # depth-dependent term tanh(k*depth)
            tanhkh = tanh(self.k*depth)
            # depth-dependent term coth(k*depth) (w/o singularity)
            cothkh = 1/tanhkh[self.k != 0.]
            # depth-dependent term for Stokes drift (w/o singularity)
            if self.mu <= 2.:  # to avoid overflow
                Us_depth_coef = cosh(2*self.k[self.k != 0.]*depth) / \
                                (2*sinh(self.k[self.k != 0.]*depth)**2)
            # grid of angular frequencies (w/o singularity)
            w = sqrt(self.k[self.k != 0.]*9.81*tanhkh[self.k != 0.])
            # grid of normalized wavenumbers in the x-direction (w/o
            # singularity)
            kx_k = self.kx[self.k != 0.]/self.k[self.k != 0.]
            # grid of normalized wavenumbers in the y-direction (w/o
            # singularity)
            ky_k = self.ky[self.k != 0.]/self.k[self.k != 0.]
            # index of wavenumber equal to zero
            i0 = nonzero(self.k.ravel() == 0.)[0][0]
            # include zero in variables
            self.w = hstack((w[:i0], 0., w[i0:])).reshape(self.k.shape)
            self.kx_k = hstack((kx_k[:i0],  0., kx_k[i0:])
                               ).reshape(self.k.shape)
            self.ky_k = hstack((ky_k[:i0], 0., ky_k[i0:])
                               ).reshape(self.k.shape)
            self.ky_k = hstack((ky_k[:i0], 0., ky_k[i0:])
                               ).reshape(self.k.shape)
            self.ky_k = hstack((ky_k[:i0], 0., ky_k[i0:])
                               ).reshape(self.k.shape)
            self.cothkh = hstack((cothkh[:i0], 0., cothkh[i0:])
                                 ).reshape(self.k.shape)
            if self.mu <= 2.:  # to avoid overflow
                self.Us_depth_coef = hstack((Us_depth_coef[:i0],
                                            0.,
                                            Us_depth_coef[i0:])
                                            ).reshape(self.k.shape)
        # update the water depth and recalculate the angular frequencies
        if self.mu != kwargs.get('mu'):
            self.mu = kwargs.get('mu')
            # water depth
            depth = self.mu/self.dk
            # depth-dependent term tanh(k*depth)
            tanhkh = tanh(self.k*depth)
            # depth-dependent term coth(k*depth) (w/o singularity)
            cothkh = 1/tanhkh[self.k != 0.]
            # depth-dependent term for Stokes drift (w/o singularity)
            if self.mu <= 2.:  # to avoid overflow
                Us_depth_coef = cosh(2*self.k[self.k != 0.]*depth) / \
                                (2*sinh(self.k[self.k != 0.]*depth)**2)
            # grid of angular frequencies (w/o singularity)
            w = sqrt(self.k[self.k != 0.]*9.81*tanhkh[self.k != 0.])
            # index of wavenumber equal to zero
            i0 = nonzero(self.k.ravel() == 0.)[0][0]
            # include zero in variables
            self.w = hstack((w[:i0], 0., w[i0:])).reshape(self.k.shape)
            self.cothkh = hstack((cothkh[:i0], 0., cothkh[i0:])
                                 ).reshape(self.k.shape)
            if self.mu <= 2.:  # to avoid overflow
                self.Us_depth_coef = hstack((Us_depth_coef[:i0],
                                            0.,
                                            Us_depth_coef[i0:])
                                            ).reshape(self.k.shape)
        # get the spectral elevation amplitudes
        self.Ak = sqrt(2*Sk*self.dk**2)
        # calculate the Stokes' drift vector
        if self.mu <= 2.:  # to avoid overflow
            Us = self.Ak**2*self.w*self.Us_depth_coef
        else:
            Us = self.Ak**2*self.w
        Usx, Usy = (Us*self.kx).sum(), (Us*self.ky).sum()
        # calculate the scalar product of Stokes' drift and wavenumber vectors
        Usdotk = self.kx*Usx+self.ky*Usy
        # calculate the nonlinear angular velocity
        self.w_NL = self.w+0.5*Usdotk
        # calculate the nonlinear vertical shift
        self.z2 = 0.5*(self.Ak**2*self.k*self.cothkh).sum()

    def get_elevation(self, dt):
        '''
        Method that calculates the surface elevation from the amplitude
        spectrum according to ICWM using IFFTs. In order to take care of the
        nonlinear particle shift, the quantities are plotted on a grid
        (alpha-Usx*t, beta-Usy*t), leading the nonlinear angular frequency to
        be w_NL = w+0.5*(Usx*kx + Usy*ky).
        To avoid non-physical behavior (i.e. particles going back and forth
        according to the current steepness), the second-order velocity
        correction is implemented as a modified phase, that is updated based on
        its value at the previous time step.

        Inputs
        ----------
        dt :: float :: Time increment.
        '''
        if not hasattr(self, 'phi_k'):
            # initiate the set of phases
            # seed for random initial phases
            random.seed(self.phase_seed)
            # initial phase vector
            self.phi_k = random.rand(self.k.shape[0], self.k.shape[1])*2*pi
        else:
            # update the set of phases
            self.phi_k += self.w_NL*dt
        # calculate the complex amplitudes
        self.Ak_c = self.Ak*exp(-1j*self.phi_k)
        # shift the complex amplitudes
        Ak_shifted_kxk = 1j*self.kx_k*self.Ak_c*self.cothkh
        Ak_shifted_kyk = 1j*self.ky_k*self.Ak_c*self.cothkh
        # calculate the free surface particles' horizontal displacement
        Dx = self.elev_from_full_spectrum_3D(Ak_shifted_kxk,
                                             self.dk, self.dk)[1]
        Dy = self.elev_from_full_spectrum_3D(Ak_shifted_kyk,
                                             self.dk, self.dk)[1]
        # calculate the free surface particles' vertical displacement
        [alpha, self.beta], self.z = self.elev_from_full_spectrum_3D(
            self.Ak_c, self.dk, self.dk)
        # add horizontal displacements to reference locations
        self.x, self.y = alpha+Dx, self.beta+Dy
        # add second-order vertical displacement
        self.z += self.z2

    def get_velocity(self, coord):
        '''
        Method that calculates the surface orbital velocities and the absolute
        velocity from the amplitude spectrum according to ICWM using IFFTs.

        Inputs
        ----------
        coord :: string :: Component to calculate.
        '''
        if coord == 'all' or coord == 'x':
            # calculate the x-component of the time-derived complex amplitudes
            Ak_kxk_w = self.kx_k*self.w_NL*self.Ak_c*self.cothkh
            # calculate the x-component of the orbital velocity
            self.U = self.elev_from_full_spectrum_3D(Ak_kxk_w,
                                                     self.dk, self.dk)[1]
        if coord == 'all' or coord == 'y':
            # calculate the y-component of the time-derived complex amplitudes
            Ak_kyk_w = self.ky_k*self.w_NL*self.Ak_c*self.cothkh
            # calculate the y-component of the orbital velocity
            self.V = self.elev_from_full_spectrum_3D(Ak_kyk_w,
                                                     self.dk, self.dk)[1]
        if coord == 'all' or coord == 'z':
            # calculate the z-component of the time-derived complex amplitudes
            Ak_shifted_w = -1j*self.w_NL*self.Ak_c
            # calculate the z-component of the orbital velocity
            self.W = self.elev_from_full_spectrum_3D(Ak_shifted_w,
                                                     self.dk, self.dk)[1]
        if coord == 'all':
            # calculate the absolute particles' velocity
            self.vel = sqrt(self.U**2+self.V**2+self.W**2)

    def get_slope(self, coord):
        '''
        Method that calculates the surface slopes and the absolute slope from
        the amplitude spectrum according to ICWM using IFFTs.

        Inputs
        ----------
        coord :: string :: Component to calculate.
        '''
        if coord == 'all' or coord == 'x':
            # calculate the x-gradient of the complex amplitudes
            Ak_shifted_kx = 1j*self.kx*self.Ak_c
            # calculate the x-component of the slope
            self.sx = self.elev_from_full_spectrum_3D(Ak_shifted_kx,
                                                      self.dk, self.dk)[1]
        if coord == 'all' or coord == 'y':
            # calculate the y-gradient of the complex amplitudes
            Ak_shifted_ky = 1j*self.ky*self.Ak_c
            # calculate the y-component of the slope
            self.sy = self.elev_from_full_spectrum_3D(Ak_shifted_ky,
                                                      self.dk, self.dk)[1]
        if coord == 'all':
            # calculate the absolute particles' velocity
            self.slope = sqrt(self.sx**2+self.sy**2)

    def elev_from_full_spectrum_3D(self, A, dkx, dky):
        '''
        Method that generates the surface elevation field from the one-sided
        surface elevation spectrum based on the 2DIFFT. Also useful for any
        regular 2DIFFT that fits the formalism.

        Inputs
        ----------
        A :: 2darray :: Wavenumber surface elevation spectrum, array of complex
                        numbers.
        dkx :: float :: Wavenumber step of the spectrum along the x-direction.
        dky :: float :: Wavenumber step of the spectrum along the y-direction.

        Outputs
        -------
        elev :: 2darray :: Regularly sampled real data (elevation).
        space :: 2darrays :: Spatial grids associated with the elevation field.
        '''
        # get the dimensions (NUMPY CONVENTION)
        nx, ny = A.shape[0], A.shape[1]
        # shift the frequencies to match the numpy formalism
        mat = fft.ifftshift(A)
        # perform the IFFT
        elev = real(fft.ifft2(mat, axes=(0, 1), norm=None))*nx*ny
        # calculate the lengths of the output signal
        Lx, Ly = 2*pi/dkx, 2*pi/dky
        # calculate the signal discretizations
        dx, dy = Lx/nx, Lx/ny
        # calculate the spatial grids
        space = meshgrid(linspace(0., Lx-dx, nx),
                         linspace(0., Ly-dy, ny),
                         indexing='ij')
        return space, elev

    def directional_spreading_cos_no_scaling(self, theta, s, theta_dir=0.):
        '''
        Method that calculates the directional spreading function from the
        wave directions of propagation according to a cos^{2s} formulation. The
        spreading function is then multiplied to the wavenumber spectrum to
        obtain the directional wave spectrum.

        Inputs
        ----------
        theta :: 2darray :: Wave directions of propagation in gradients.
        s :: float :: Directional spreading factor.
        theta_dir :: float :: Main direction of propagation in gradients.

        Outputs
        ----------
        G :: 2darray :: Spreading function.
        '''
        # calculate the cos^{2s} spreading function
        G = abs(cos(0.5*(theta-theta_dir)))**(2*s)
        return G
