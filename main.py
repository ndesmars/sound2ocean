# -*- coding: utf-8 -*-
'''
Real-time and interactive weakly nonlinear ocean wave field simulations from a
sound signal.

Through an interactive GUI, the user can modify in live some wave spectrum and
visualization parameters.

Qt and OpenGL python bindings are extensively used through PyQtGraph.

For more details than those found in the documentation, code comments and
references, please let me know at:
    nicolas _dot_ desmars _at_ protonmail _dot_ com

License
----------
Copyright 2024 Nicolas Desmars

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import yaml
from OceanSurface import OceanSurface
from InteractiveApp import InteractiveApp
import PyQt5
from pyqtgraph.Qt import QtCore
import pyaudio
import sys
from scipy.io import wavfile
from numpy import float32, int16
from queue import Queue


if len(sys.argv) == 3:
    # if an initialization file is given as argument
    # get initial parameters from specified initialization file
    init_param_path = sys.argv[2]
    with open(init_param_path, 'r') as yamlFile:
        parsed_init_param = yaml.full_load(yamlFile)
    fps = parsed_init_param['fps']
    Nk = parsed_init_param['Nk']
    i_full_screen = parsed_init_param['i_full_screen']
    i_multiple = parsed_init_param['i_multiple']
    phase_seed = parsed_init_param['phase_seed']
    Tmax = parsed_init_param['Tmax']
    coef_steep = parsed_init_param['coef_steep']
    coef_smooth = parsed_init_param['coef_smooth']
    coef_spread = parsed_init_param['coef_spread']
    coef_dir = parsed_init_param['coef_dir']
    coef_damp = parsed_init_param['coef_damp']
    coef_filter = parsed_init_param['coef_filter']
    coef_disp = parsed_init_param['coef_disp']
    plotting_style = parsed_init_param['plotting_style']
    mds = parsed_init_param['mds']
    pds = parsed_init_param['pds']
    point_size = parsed_init_param['point_size']
    mapped_quantity = parsed_init_param['mapped_quantity']
    colormap = parsed_init_param['colormap']
    hue = parsed_init_param['hue']
    sat = parsed_init_param['sat']
    val = parsed_init_param['val']
    vmin = parsed_init_param['vmin']
    vmax = parsed_init_param['vmax']
    fov = parsed_init_param['fov']
    elevation = parsed_init_param['elevation']
    azimuth = parsed_init_param['azimuth']
    center = parsed_init_param['center']
    distance = parsed_init_param['distance']
    # create an instance of wave field simulation
    OS = OceanSurface(Nk=Nk, phase_seed=phase_seed)
    # create a Qt application
    app = PyQt5.QtWidgets.QApplication([])
    # create widget of custom GUI
    IA = InteractiveApp(OS, fps=fps, Tmax=Tmax, coef_steep=coef_steep,
                        coef_smooth=coef_smooth, coef_spread=coef_spread,
                        coef_dir=coef_dir, coef_damp=coef_damp,
                        coef_filter=coef_filter, coef_disp=coef_disp,
                        i_multiple=i_multiple, plotting_style=plotting_style,
                        mds=mds, pds=pds, point_size=point_size,
                        mapped_quantity=mapped_quantity, colormap=colormap,
                        hue=hue, sat=sat, val=val, vmin=vmin, vmax=vmax,
                        fov=fov, elevation=elevation, azimuth=azimuth,
                        center=center, distance=distance)
elif len(sys.argv) == 2:
    # if no initialization file is given
    # set default values for i_full_screen
    i_full_screen = False
    # create an instance of wave field simulation
    OS = OceanSurface(i_jonswap=False)
    # create a Qt application
    app = PyQt5.QtWidgets.QApplication([])
    # create widget of custom GUI
    IA = InteractiveApp(OS)
else:
    # exit if more than two arguments are given
    sys.exit('Please give an input WAV file. An optional initilization file '
             'can be given as second argument.')
# show the GUI
IA.show()
if i_full_screen:
    # to display the GUI in full screen
    IA.showFullScreen()
# load sound data from specified initialization file
wav_file_path = sys.argv[1]
# get data and corresponding rate
rate, data = wavfile.read(wav_file_path)
# if data is of type int16, convert the format to float32
if data.dtype == int16:
    data = data.astype(float32, order='C')/32768.
# set the a queue with infinite size for the sound signal
sound_queue = Queue(maxsize=0)
# put sequences of sound data in the queue:
# one sequence is stream each time the callback function of the sound stream
# is called
frame_count = 1024  # defines the length of one sound sequence
# get the number of sequence in the full sound signal
n_loop = data.shape[0]//frame_count
# loop over the signal to feed the queue
for ii in range(n_loop):
    sound_queue.put(data[ii*frame_count:(ii+1)*frame_count])
# import sound data to the InteractiveApp instance
IA.import_file_data(sound_queue)
# create a pyaudio instance
p = pyaudio.PyAudio()
# open the sound stream
stream = p.open(format=pyaudio.paFloat32,
                frames_per_buffer=1024,
                channels=data.ndim,
                rate=rate,
                output=True,
                input=False,
                stream_callback=IA.stream_callback_file)
# create Qt timer
t = QtCore.QTimer()
# connect it to the time-update function of GUI
t.timeout.connect(IA.update_time)
# calculate refresh rate of the image
image_refresh_rate = int(1./IA.fps*1000)
# start the time updates
t.start(image_refresh_rate)
if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        PyQt5.QtWidgets.QApplication.instance().exec_()
