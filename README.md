# sound2ocean

Real-time and interactive weakly nonlinear ocean wave field simulations from a sound signal. Demonstrative videos available [here](https://www.youtube.com/watch?v=nYxiBD7rSBE), [here](https://www.youtube.com/watch?v=ThtZ95ht164) and [here](https://www.youtube.com/watch?v=ON2xMb464Pk). If you like this project, you may be interested in [ocean2sound](https://gitlab.com/ndesmars/ocean2sound).

Through an interactive interface, the user can modify in live some wave field and visualization parameters.

Qt and OpenGL python bindings are extensively used through [PyQtGraph](http://www.pyqtgraph.org/).

The project documentation consists of this README file and the [PDF document](https://gitlab.com/ndesmars/sound2ocean/-/blob/master/formulae_of_surface_quantities.pdf) 'formulae_of_surface_quantities'. Also the code is substantially commented, so refer to it for more implementation details and to the references for further theoretical considerations.

## Dependencies

The following Python packages are required:

- [NumPy](https://numpy.org/)
- [SciPy](https://scipy.org/)
- [YAML](https://yaml.org/)
- [PyQtGraph](http://www.pyqtgraph.org/)
- [PyQt5](https://www.riverbankcomputing.com/static/Docs/PyQt5/)
- [PyOpenGL](http://pyopengl.sourceforge.net/)

Install them with pip by running

`pip install numpy scipy PyYAML pyqtgraph PyQt5 PyOpenGL PyOpenGL_accelerate`

Additionally, [PyAudio](http://people.csail.mit.edu/hubert/pyaudio/) (which provides Python bindings for [PortAudio](https://www.portaudio.com/)) is required. On macOS, it can be easily installed with homebrew and pip by running

`brew install portaudio`\
`pip install PyAudio`

For other OS, refer to the [PyAudio installation documentation](http://people.csail.mit.edu/hubert/pyaudio/#downloads).

## Running sound2ocean

To run sound2ocean with the _default_ initial simulation parameters, simply run

`python main.py audio_file.wav`

in which `audio_file.wav` is a WAV file containing the sound signal to use for the ocean wave field generation. The input file can have one or multiple channels, and the data type has to be int16 or float32.

If you want to use _custom_ initial parameters, run

`python main.py audio_file.wav initilization_file.yaml`

in which the argument `initilization_file.yaml` is a YAML file containing the initial parameters of the simulation. _All_ the initial parameters described below are required in the initialization file to properly initiate the simulation. Some parameters are fixed and cannot be changed during the simulation, some are editable through the GUI.

Examples of initialization files are provided in the folder `./init_examples`. Screenshots of the corresponding results are shown in the gallery section. The first example (`init_param_ex1.yaml`) contains the default initial parameters.

### Non-live-editable parameters
- `fps` ― float ― Number of frames per second
- `Nk` ― int ― Number of wave components along each direction in the wave model (the total number of wave components is `Nk*Nk`) ― _Note that optimal numerical performances are reached when_ `Nk` _is equal to a power of 2._
- `i_full_screen` ― bool ― Set to `True` to have the GUI in full screen
- `i_multiple` ― bool ― Set to `True` to replicate the domain multiple (`2*2 = 4`) times
- `phase_seed` ― int ―  Seed for the initial distribution of the wave phases

### Live-editable wave field parameters
- `Tmax` ― float ― Maximal wave period in seconds (period of the longest wave)
- `coef_steep` ― float ― Coefficient controling how steep the waves are ― _Note that the wave model allows the steepness to be arbitrarily high, potentially leading the simulation to be physically unrealistic._
- `coef_smooth` ― float ― Coefficient controling how smooth the wave spectrum is with respect to the time variations of the sound input
- `coef_spread` ― float ― Coefficient controling how much the wave energy is spread around the main direction of propagation of the wave field
- `coef_dir` ― float ― Coefficient controling the main direction of propagation of the wave field
- `coef_damp` ― float ― Coefficient controling how much high-frequency wave components are damped
- `coef_filter` ― float ― Coefficient controling a high-pass filter of the wave components
- `coef_disp` ― float ― Coefficient controling how dispersive the waves are

### Live-editable visualization parameters
- `plotting_style` ― string ― Plotting style, in `{mesh, points}`
- `colormap` ― string ― Colormap name, in `{haze_cyan, inferno, magma, PiYG, erdc_blue2cyan_BW, viridis, parula, x_ray, RF, LF, ASJ}` (add `_r` for the reversed version, e.g., `magma_r`)
- `mapped_quantity` ― string ― Quantity mapped by the colormap, in `{elevation, velocity x, velocity y, velocity z, absolute velocity, slope x, slope y, absolute slope}` ― _Note that the elevation and velocities are normalized such that the resulting fields approximately range between -1 and 1. The slopes are not normalized._
- `mds` ― string ― Mesh display style (only used if `plotting_style=mesh`), in `{faces, wireframe}`
- `pds` ― string ― Points display style (only used if `plotting_style=points`), in `{additive, translucent, opaque}`
- `point_size` ― float ― Point size (only used if plot style is points)
- `vmin` ― float ― Minimal colormap value
- `vmax` ― float ― maximal colormap value
- `hue` ― float ― Hue correction of the colormap, in `[0., 1.]`
- `sat` ― float ― Saturation correction of the colormap, in `[-1., 1.]`
- `val` ― float ― Value (lightness) correction of the colormap,  in `[-1., 1.]`
- `fov` ― float ― Field of view of the camera in degrees
- `elevation` ― float ― Elevation angle the camera in degrees
- `azimuth` ― float ― Azimuth angle of the camera in degrees
- `center`― 1darray ― x-, y-, z-coordinates (normalized by the peak wavelength) of the center point of the camera's view
- `distance` ― float ― Distance (normalized by the peak wavelength) of the camera from the center point

## Gallery

A demonstrative video, showing the effect of the live modification of wave field parameters, is available [here](https://www.youtube.com/watch?v=nYxiBD7rSBE).

Other videos using different input audio signals and visualization settings are available [here](https://www.youtube.com/watch?v=ThtZ95ht164) and [here](https://www.youtube.com/watch?v=ON2xMb464Pk).

Examples of generated wave field:

<img src="gallery/example1.png" alt="example1" width="800"/>

<img src="gallery/example3.png" alt="example3" width="800"/>

<img src="gallery/example2.png" alt="example2" width="800"/>

## Reference

The ocean surface is modeled according to the Improved Choppy Wave Model:

Guérin, C.-A., Desmars, N., Grilli, S. T., Ducrozet, G., Perignon, Y. & Ferrant, P. (2019) An improved Lagrangian model for the time evolution of nonlinear surface waves. _Journal of Fluid Mechanics_ 876, 527–552. [[PDF]](https://hal.archives-ouvertes.fr/hal-02310305/document)

## License

Copyright 2024 Nicolas Desmars

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
