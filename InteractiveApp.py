# -*- coding: utf-8 -*-
'''
For more details than those found in the documentation, code comments and
references, please let me know at:
    nicolas _dot_ desmars _at_ protonmail _dot_ com

License
----------
Copyright 2024 Nicolas Desmars

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import pyaudio
from CustomColorMaps import CustomColorMaps
from numpy import stack, vstack, hstack, log10, array,  moveaxis, \
    concatenate, zeros, newaxis, geomspace, linspace, fft, meshgrid, arctan2, \
    hypot, interp, random, pi
from PyQt5.QtWidgets import QLabel, QSizePolicy, QSlider, QSpacerItem, \
    QVBoxLayout, QWidget, QGridLayout, QComboBox
from pyqtgraph import Qt, opengl, ColorMap, Vector
from queue import Queue
from scipy.signal import welch
import sys


class ComboBox(QWidget):
    '''
    Class that defines the Qt comboboxes that are used in the GUI.
    '''

    def __init__(self, string_list, p_full_name, default_kw, **kwargs):
        '''
        Constructor.

        Inputs
        ----------
        string_list :: list :: List of strings to be displayed by the combobox
                               when selecting the item.
        p_full_name :: string :: Full name of the combobox's list of items.
        default_kw :: string :: Key word of the combobox's item that is
                                selected by default.

        kwargs
        ----------
        kw_list :: list :: List of key words (strings) referring to the
                           combobox's items.
        '''
        super(ComboBox, self).__init__(parent=None)
        # get the optional key-word list
        self.kw_list = kwargs.get('kw_list', string_list)
        # create vertical layout
        verticalLayout = QVBoxLayout(self)
        # add space on the top of the label
        spacerItemL = QSpacerItem(0, -5, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # create and add a label
        label = QLabel(self)
        verticalLayout.addWidget(label)
        label.setText(p_full_name)
        label.setAlignment(Qt.QtCore.Qt.AlignCenter)
        # add space on the top of the combobox
        spacerItemL = QSpacerItem(0, -5, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # create and add combobox
        self.combobox = QComboBox(self)
        self.combobox.addItems(string_list)
        verticalLayout.addWidget(self.combobox)
        # add space on the bottom of the combobox
        spacerItemL = QSpacerItem(0, -5, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # get name of the default item
        default_text = string_list[self.kw_list.index(default_kw)]
        # set default value of the combobox
        index = self.combobox.findText(default_text,
                                       Qt.QtCore.Qt.MatchFixedString)
        self.combobox.setCurrentIndex(index)
        # init state (value modified by the user or not) to False
        self.state = False


class Slider(QWidget):
    '''
    Class that defines the Qt sliders that are used in the GUI.
    '''

    def __init__(self, p_name, p_full_name, default_x, minimum, maximum,
                 i_multiple=False):
        '''
        Constructor.

        Inputs
        ----------
        p_name :: string :: Short name of the slider's parameter.
        p_full_name :: string :: Full name of the slider's parameter.
        default_x :: float :: Default value the slider's parameter.
        minimum :: float :: Minimum value the slider's parameter.
        maximum :: float :: Maximum value the slider's parameter.
        i_multiple :: bool :: If True, the domain is replicated multiple times.
        '''
        super(Slider, self).__init__(parent=None)
        self.i_multiple = i_multiple
        # create vertical layout
        verticalLayout = QVBoxLayout(self)
        # add space on the top of the label
        spacerItemL = QSpacerItem(0, -10, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # create and add label (slider value)
        self.label = QLabel(self)
        verticalLayout.addWidget(self.label)
        # add space on the top of the slider
        spacerItemL = QSpacerItem(0, -10, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # create and add the slider
        self.slider = QSlider(self)
        self.slider.setOrientation(Qt.QtCore.Qt.Horizontal)
        # add space on the bottom
        verticalLayout.addWidget(self.slider)
        spacerItemR = QSpacerItem(0, -10, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemR)
        # adapt label value when the value is modified
        self.p_full_name = p_full_name
        self.minimum = minimum
        self.maximum = maximum
        if p_name in {'Tmax'}:
            # to have a logarithmical evolution
            self.i_log = True
        else:
            # to have a linear evolution
            self.i_log = False
        v = self.get_value_from_x(default_x)
        self.slider.setValue(v)
        self.slider.valueChanged.connect(self.set_label_value)
        self.set_label_value(self.slider.value())
        # init state (value modified by the user or not) to False
        self.state = False

    def get_value_from_x(self, x):
        '''
        Method that gets the value (from 0 to 100 %) of the slider from the
        input (dimensional value of the parameter).

        Inputs
        ----------
        x :: float :: Dimensional value of the slider's parameter.

        Outputs
        ----------
        value :: int :: Value in percent of the slider's parameter.
        '''
        if self.i_log:
            # to have a logarithmical evolution
            value = int(self.slider.minimum() +
                        ((log10(x)-log10(self.minimum)) /
                         (log10(self.maximum)-log10(self.minimum))
                         )*(self.slider.maximum()-self.slider.minimum()))
        else:
            # to have a linear evolution
            value = int(self.slider.minimum() +
                        ((x-self.minimum) /
                         (self.maximum-self.minimum)
                         )*(self.slider.maximum()-self.slider.minimum()))
        return value

    def set_label_value(self, value):
        '''
        Method that sets the slider's label according to its current value
        (the parameter 'value' is the % of the slider).

        Inputs
        ----------
        value :: int :: Value in percent of the slider's parameter.
        '''
        # calculate the dimensional parameter from the value
        if self.i_log:
            # if a logarithmical evolution is used
            x = log10(self.minimum) + \
                (float(value)/(self.slider.maximum()-self.slider.minimum())
                 )*(log10(self.maximum)-log10(self.minimum))
            self.x = 10**x
        else:
            # if a linear evolution is used
            self.x = self.minimum + \
                (float(value)/(self.slider.maximum()-self.slider.minimum())
                 )*(self.maximum-self.minimum)
        # print the new value
        if self.i_multiple and \
                self.p_full_name == 'number of peak wavelengths':
            # adapt the printed number of peak periods if the domain is
            # replicated multiple (2*2 = 4) times
            self.label.setText(self.p_full_name + r': {:.2f}'.format(2*self.x))
        elif self.p_full_name == 'characteristic steepness':
            self.label.setText(self.p_full_name +
                               r': {:.2f} [%]'.format(100*self.x))
        else:
            self.label.setText(self.p_full_name + r': {:.2f}'.format(self.x))
        # center the label
        self.label.setAlignment(Qt.QtCore.Qt.AlignCenter)


class InteractiveApp(QWidget):
    '''
    Class that defines the Qt GUI containing wave field visualization, sliders
    and comboboxes.
    '''

    def __init__(self, OS, **kwargs):
        '''
        Constructor.

        Inputs
        ----------
        OS :: OceanSurface :: Wave field simulation.

        kwargs
        ----------
        fps :: float :: Number of frames per second.
        i_multiple :: bool :: If True, the domain is replicated multiple times.
        Tmax :: float :: Maximal period of the wave field in seconds.
        coef_steep :: float :: Coefficient for the characteristic steepness
                               calculation, in [0., 1.].
        coef_smooth :: float :: Coefficient for the smoothing (averaging) of
                                the wave spectrum steepness calculation, in
                                [0., 1.].
        coef_spread :: float :: Coefficient for the directional spreading of
                                the wave field, in [0., 1.].
        coef_dir :: float :: Coefficient for the main direction of propagation
                             of the wave field, in [0., 1.].
        coef_damp :: float :: Coefficient for the damping of the high-frequency
                              components, in [0., 1.].
        coef_filter :: float :: Coefficient for the high-pass filtering of the
                                wave spectrum, in [0., 1.].
        coef_disp :: float :: Coefficient for the characteristic dispersion
                              (non-dimensional water depth) calculation, in
                              [0., 1.].
        plotting_style :: string :: What to plot, 'mesh' or 'points'.
        mds :: string :: Mesh display style, 'wireframe' or 'faces'.
        pds :: string :: Points display style, one of the setGLOptions.
        point_size :: float :: Size of the displayed points
        mapped_quantity :: string :: Quantity mapped by the colormap,
                                     'elevation', 'veloctiy x', 'veloctiy y',
                                     'veloctiy z', 'absolute veloctiy',
                                     'slope x', 'slope y' or 'absolute slope'.
        colormap :: string :: Colormap, see the available colormaps provided by
                              the class CustomColorMaps.
        hue :: float :: Hue correction applied to the colormap, in [0., 1.].
        sat :: float :: Saturation correction applied to the colormap, in
                        [-1., 1.].
        val :: float :: Value (lightness) correction applied to the colormap,
                        in [-1., 1.].
        vmin :: float :: Mininmal colormap value.
        vmax :: float :: Maxinmal colormap value.
        fov :: float :: Field of view of the camera in degrees.
        elevation :: float :: Elevation angle the camera in degrees.
        azimuth :: float :: Azimuth angle of the camera in degrees.
        center :: 1darray :: Center point's coordinates in meters of the
                             camera's view. Values are normalized by the peak
                             wavelength.
        distance :: float :: Distance in meters of the camera from the center
                             point. The value is normalized by the peak
                             wavelength.
        '''
        super(InteractiveApp, self).__init__(parent=None)
        # assign the instance the input wave field simulation
        self.OS = OS
        # get the optional parameters (except camera parameters)
        self.fps = kwargs.get('fps', 25.)
        self.i_multiple = kwargs.get('i_multiple', False)
        self.Tmax = kwargs.get('Tmax', 10.)
        coef_steep = kwargs.get('coef_steep', 0.5)
        coef_smooth = kwargs.get('coef_smooth', 0.4)
        coef_spread = kwargs.get('coef_spread', 0.5)
        coef_dir = kwargs.get('coef_dir', 0.5)
        coef_damp = kwargs.get('coef_damp', 0.)
        coef_filter = kwargs.get('coef_filter', 0.)
        coef_disp = kwargs.get('coef_disp', 1.)
        self.plotting_style = kwargs.get('plotting_style', 'mesh')
        self.mds = kwargs.get('mds', 'faces')
        self.pds = kwargs.get('pds', 'additive')
        self.point_size = kwargs.get('point_size', 3.)
        self.mapped_quantity = kwargs.get('mapped_quantity', 'elevation')
        self.colormap = kwargs.get('colormap', 'LF')
        self.hue = kwargs.get('hue', 0.)
        self.sat = kwargs.get('sat', 0.)
        self.val = kwargs.get('val', 0.)
        self.vmin = kwargs.get('vmin', -0.75)
        self.vmax = kwargs.get('vmax', 0.5)
        # set the size of the GUI window
        self.resize(1250, 900)
        # create the layout
        gridLayout = QGridLayout(self)
        # size of the wave field plot (number of lines and columns in the GUI)
        nlin, ncol = 18, 18
        # create the openGL widget for the 3D visualization
        self.w = opengl.GLViewWidget()
        # set the camera position and viewing parameters
        self.w.opts['fov'] = kwargs.get('fov', 60.)
        self.w.opts['elevation'] = kwargs.get('elevation', 90.)
        self.w.opts['azimuth'] = kwargs.get('azimuth', 180.)
        self.w.opts['center'] = Vector(kwargs.get('center', [0.5, 0.5, 0.]))
        self.w.opts['distance'] = kwargs.get('distance', 0.86)
        # add the 3D plot widget to the layout
        gridLayout.addWidget(self.w, 1, 1, nlin, ncol)
        # index of line for widgets
        ii = 1
        # add sliders for wave parameters
        # maximal period
        self.sl_Tmax = Slider(r'Tmax', r'maximal period [s]',
                              self.Tmax, 0.1, 100.)
        gridLayout.addWidget(self.sl_Tmax, ii, ncol+1, 1, 2)
        ii += 1
        # steepness coefficient
        self.sl_steep = Slider(r'steep', r'steepness',
                               coef_steep, 0., 1.)
        gridLayout.addWidget(self.sl_steep, ii, ncol+1, 1, 2)
        ii += 1
        # spectrum smoothing coefficient
        self.sl_smooth = Slider(r'smooth', r'spectrum smoothing',
                                coef_smooth, 0., 1.)
        gridLayout.addWidget(self.sl_smooth, ii, ncol+1, 1, 2)
        ii += 1
        # directional spreading coefficient
        self.sl_spread = Slider(r'spread', r'directional spreading',
                                coef_spread, 0., 1.)
        gridLayout.addWidget(self.sl_spread, ii, ncol+1, 1, 2)
        ii += 1
        # direction of propagation coefficient
        self.sl_dir = Slider(r'dir', r'direction of propagation',
                             coef_dir, 0., 1.)
        gridLayout.addWidget(self.sl_dir, ii, ncol+1, 1, 2)
        ii += 1
        # high-frequency damping coefficient
        self.sl_damp = Slider(r'damp', r'high-frequency damping',
                              coef_damp, 0., 1.)
        gridLayout.addWidget(self.sl_damp, ii, ncol+1, 1, 2)
        ii += 1
        # high-pass filter coefficient
        self.sl_filter = Slider(r'filter', r'high-pass filter',
                                coef_filter, 0., 1.)
        gridLayout.addWidget(self.sl_filter, ii, ncol+1, 1, 2)
        ii += 1
        # dispersion coefficient
        self.sl_disp = Slider(r'disp', r'dispersion',
                              coef_disp, 0., 1.)
        gridLayout.addWidget(self.sl_disp, ii, ncol+1, 1, 2)
        ii += 1
        # add comboboxes for some image parameters
        # ploting style
        self.cb_pls = ComboBox([r'mesh', r'points'],
                               r'ploting style ', self.plotting_style)
        gridLayout.addWidget(self.cb_pls, ii, ncol+1, 1, 1)
        # colormap
        self.cb_cm = ComboBox([r'haze_cyan', r'haze_cyan_r',
                               r'inferno', r'inferno_r',
                               r'magma', r'magma_r',
                               r'PiYG', r'PiYG_r',
                               r'erdc_blue2cyan_BW', r'erdc_blue2cyan_BW_r',
                               r'viridis', r'viridis_r',
                               r'parula', r'parula_r',
                               r'x_ray', r'x_ray_r',
                               r'RF', r'RF_r',
                               r'LF', r'LF_r',
                               r'ASJ', r'ASJ_r'],
                              r'colormap', self.colormap)
        gridLayout.addWidget(self.cb_cm, ii, ncol+2, 1, 1)
        ii += 1
        # quantity to map
        self.cb_mq = ComboBox([r'free surface elevation η/Hₛ',
                               r'orbital velocity x-component U/(4σᴜ)',
                               r'orbital velocity y-component V/(4σᴠ)',
                               r'orbital velocity z-component W/(4σᴡ)',
                               r'absolute orbital velocity (v-μᵥ)/(4σᵥ) ' +
                               r'with v²=U²+V²+W²',
                               r'slope x-direction',
                               r'slope y-direction',
                               r'absolute slope'],
                              r'mapped quantity', self.mapped_quantity,
                              kw_list=[r'elevation', r'velocity x',
                                       r'velocity y', r'velocity z',
                                       r'absolute velocity', r'slope x',
                                       r'slope y', r'absolute slope'])
        gridLayout.addWidget(self.cb_mq, ii, ncol+1, 1, 2)
        ii += 1
        # mesh display style
        self.cb_mds = ComboBox([r'wireframe', r'faces'],
                               r'mesh display style', self.mds)
        gridLayout.addWidget(self.cb_mds, ii, ncol+1, 1, 1)
        # points display style
        self.cb_pds = ComboBox([r'opaque', r'translucent', r'additive'],
                               r'points display style', self.pds)
        gridLayout.addWidget(self.cb_pds, ii, ncol+2, 1, 1)
        ii += 1
        # add sliders for other image parameters
        # point size
        self.sl_pts = Slider(r'pts', r'point size [px]',
                             self.point_size, 0.1, 20)
        gridLayout.addWidget(self.sl_pts, ii, ncol+1, 1, 2)
        ii += 1
        # mininmal colormap value
        self.sl_vmin = Slider(r'vmin', r'mininmal colormap value',
                              self.vmin, -1.5, 1.5)
        gridLayout.addWidget(self.sl_vmin, ii, ncol+1, 1, 2)
        ii += 1
        # mininmal colormap value
        self.sl_vmax = Slider(r'vmax', r'maximal colormap value',
                              self.vmax, -1.5, 1.5)
        gridLayout.addWidget(self.sl_vmax, ii, ncol+1, 1, 2)
        ii += 1
        # hue correction
        self.sl_hue = Slider(r'hue', r'hue correction',
                             self.hue, 0., 1.)
        gridLayout.addWidget(self.sl_hue, ii, ncol+1, 1, 2)
        ii += 1
        # saturation correction
        self.sl_sat = Slider(r'sat', r'saturation correction',
                             self.sat, -1., 1.)
        gridLayout.addWidget(self.sl_sat, ii, ncol+1, 1, 2)
        ii += 1
        # value (lightness) correction
        self.sl_val = Slider(r'val', r'value (lightness) correction',
                             self.val, -1., 1.)
        gridLayout.addWidget(self.sl_val, ii, ncol+1, 1, 2)
        ii += 1
        # add a label to print the camera parameters
        self.get_text_cam_params()
        self.lb_cam = QLabel(self.cam_params)
        # center the text
        self.lb_cam.setAlignment(Qt.QtCore.Qt.AlignCenter)
        gridLayout.addWidget(self.lb_cam, ii, ncol+1, 1, 2)
        # update if sliders' values has been modified
        self.sl_Tmax.slider.valueChanged.connect(
            self.set_sl_Tmax_state_to_true)
        self.sl_steep.slider.valueChanged.connect(
            self.set_sl_steep_state_to_true)
        self.sl_smooth.slider.valueChanged.connect(
            self.set_sl_smooth_state_to_true)
        self.sl_spread.slider.valueChanged.connect(
            self.set_sl_spread_state_to_true)
        self.sl_dir.slider.valueChanged.connect(
            self.set_sl_dir_state_to_true)
        self.sl_damp.slider.valueChanged.connect(
            self.set_sl_damp_state_to_true)
        self.sl_filter.slider.valueChanged.connect(
            self.set_sl_filter_state_to_true)
        self.sl_disp.slider.valueChanged.connect(
            self.set_sl_disp_state_to_true)
        self.sl_pts.slider.valueChanged.connect(
            self.set_sl_pts_state_to_true)
        self.sl_vmin.slider.valueChanged.connect(
            self.set_sl_vmin_state_to_true)
        self.sl_vmax.slider.valueChanged.connect(
            self.set_sl_vmax_state_to_true)
        self.sl_hue.slider.valueChanged.connect(self.set_sl_hue_state_to_true)
        self.sl_sat.slider.valueChanged.connect(self.set_sl_sat_state_to_true)
        self.sl_val.slider.valueChanged.connect(self.set_sl_val_state_to_true)
        # update if comboboxes' values have been modified
        self.cb_pls.combobox.currentTextChanged.connect(
            self.set_cb_pls_state_to_true)
        self.cb_cm.combobox.currentTextChanged.connect(
            self.set_cb_cm_state_to_true)
        self.cb_mq.combobox.currentTextChanged.connect(
            self.set_cb_mq_state_to_true)
        self.cb_mds.combobox.currentTextChanged.connect(
            self.set_cb_mds_state_to_true)
        self.cb_pds.combobox.currentTextChanged.connect(
            self.set_cb_pds_state_to_true)
        # initiate the wave field 3D plot
        self.init_plot()

    def init_plot(self):
        '''
        Method that initiates the 3D plot.
        '''
        # initiate the sound data
        # set the number of frames (i.e. chunk analyses) for spectrum averaging
        self.n_frames = 500
        # create the queue for the sound signal
        self.sound = Queue(maxsize=self.n_frames)
        # fill the queue with random data for the beginning
        for ii in range(self.n_frames):
            self.sound.put(random.rand(1024))
        # get the initial smoothing ramp
        self.update_spectrum_smoothing()
        # get the initial high-pass filter coefficient
        self.update_high_pass_filter_coefficient()
        # get the initial sound spectrum
        amp = self.calculate_sound_spectrum()
        # calculate the initial wavenumber grid properties
        self.update_wavenumber_grid()
        # calculate the initial wavenumber spectrum
        E = interp(self.k.ravel(), self.k_amp, amp).reshape(self.k.shape)
        # get the initial directional spreading function
        self.update_directional_spreading()
        # calculate the initial significant wave height
        self.update_wave_height()
        # calculate the initial directional wavenumber spectrum
        Sk = abs(E*self.G)
        # scale the spectrum
        Sk *= (self.Hs**2/16)/(Sk.sum()*self.dk**2)
        # get the initial dispersion
        self.update_dispersion()
        # get the initial spectral amplitudes
        self.OS.get_modal_amplitudes_external_input(Sk, self.k_vec, mu=self.mu)
        # initiate the time index
        self.time_index = 0
        # calculate the time step according to the fps
        self.dt = 1./self.fps
        # get the initial surface elevation
        self.OS.get_elevation(self.dt)
        # create an instance of custom colormaps
        self.CCM = CustomColorMaps()
        # load the initial colormap
        cm_params = self.CCM.load(self.cb_cm.combobox.currentText())
        # apply the intial HSV corrections
        cm_params = self.CCM.apply_correction(cm_params,
                                              self.sl_hue.x,
                                              self.sl_sat.x,
                                              self.sl_val.x)
        # create the colormap
        self.cm = ColorMap(cm_params[:, 0], cm_params[:, 1:])
        # set the 'modified plotting style state' to False
        self.mod_style_state = False
        # plot the initial data
        self.plot_data()

    def update_time(self):
        '''
        Method that updates the time-dependent parameters of the simulation.
        '''
        # increment index of current time
        self.time_index += 1
        # calculate the time in seconds
        time = self.time_index/self.fps
        # print the current time
        sys.stdout.write('\r' + self.get_time(time))
        sys.stdout.flush()
        # update the parameters of the wave field (and plotting properties)
        self.update_wave_field_params()
        # update the sound spectrum
        amp = self.calculate_sound_spectrum()
        # calculate the wavenumber spectrum by interpolating the sound spectrum
        E = interp(self.k.ravel(), self.k_amp, amp).reshape(self.k.shape)
        # calculate the directional wavenumber spectrum
        Sk = abs(E*self.G)
        # scale the spectrum
        Sk *= (self.Hs**2/16)/(Sk.sum()*self.dk**2)
        # update the spectral amplitudes
        self.OS.get_modal_amplitudes_external_input(Sk, self.k_vec, mu=self.mu)
        # get the current surface elevation
        self.OS.get_elevation(self.dt)
        # update the 3D plot
        self.plot_data()
        # update print of camera parameters
        self.get_text_cam_params()
        self.lb_cam.setText(self.cam_params)

    def calculate_sound_spectrum(self):
        '''
        Method that calculate the spectral content of the sound signal. It uses
        the Welch method and a ramping function to smooth out the evolutions
        of the sound spectrum.

        Outputs
        ----------
        amp_norm :: 1darray :: Normalized spectral amplitudes of the sound
                               signal.
        '''
        if not hasattr(self, 'damping_func'):
            # define a damping function for the high-frequency components
            self.update_high_frequency_damping()
        # get the content of the sound signal into a numpy array
        arr = array(self.sound.queue)
        # apply a ramping function making the latest recorded signals
        # predominant over the previously recorded ones
        arr = (arr*self.ramp[:arr.shape[0], newaxis])
        # calculate the spectral amplitudes
        amp = abs(welch(arr.ravel()[::2],
                        fs=44100//2,
                        window='hamming',
                        nperseg=self.OS.Nk,
                        return_onesided=False,
                        noverlap=self.OS.Nk-self.OS.Nk//2)[1])[:self.OS.Nk//2]
        # square the amplitudes for better distinction between frequencies
        amp_norm = amp**2
        # apply a damping function for the high-frequency components
        amp_norm *= self.damping_func
        # make the lowest amplitude equal to zero
        amp_norm -= amp_norm.min()
        # make sure the mean is zero
        amp_norm[0] = 0.
        # make the highest amplitude equal to one
        amp_norm /= amp_norm.max()
        # apply the high-pass filter
        amp_norm = self.high_pass_filter_spectrum(amp_norm)
        return amp_norm

    def high_pass_filter_spectrum(self, amp):
        '''
        Method that applies a high-pass filter on the frequency spectrum. This
        filter only consists in a binary operator on spectral amplitudes.

        Inputs
        ----------
        amp :: 1darray :: Input spectral amplitudes.

        Outputs
        ----------
        amp_filtered :: 1darray :: Filtered spectral amplitudes.
        '''
        # calculate the number of frequency components to filter out
        N = round(0.1*self.coef_filter*len(amp))
        amp_filtered = amp
        # set the amplitude of filtered-out components to zero
        amp_filtered[1:N] = 0.
        return amp_filtered

    def update_wave_field_params(self):
        '''
        Method that updates the modified wave field parameters. It checks if
        the state of the parameters have been modified, updates the impacted
        quantities, and sets the corresponding states to False.
        '''
        # check if the mapped quantity of the visualization has been modified
        if self.cb_mq.state:
            self.update_mapped_quantity()
            self.cb_mq.state = False
        # check if the mesh display style of the visualization has been
        # modified
        if self.cb_mds.state:
            self.update_mesh_display_style()
            self.cb_mds.state = False
        # check if the points display style of the visualization has been
        # modified
        if self.cb_pds.state:
            self.update_points_display_style()
            self.cb_pds.state = False
        # check if the point size of the visualization has been modified
        if self.sl_pts.state:
            self.update_point_size()
            self.sl_pts.state = False
        # check if the colormap limits of the visualization have been modified
        if self.sl_vmin.state or self.sl_vmax.state:
            self.update_cmap_limits()
            self.sl_vmin.state, self.sl_vmax.state = False, False
        # check if the colormap of the visualization has been modified
        if self.sl_hue.state or self.sl_sat.state or self.sl_val.state or \
                self.cb_cm.state:
            self.update_colormap()
            self.sl_hue.state, self.sl_sat.state, self.sl_val.state, \
                self.cb_cm.state = False, False, False, False
        # check if the plotting style of the visualization has been modified
        if self.cb_pls.state:
            self.update_plotting_style()
            self.cb_pls.state = False
        # check if the steepness coefficient has been modified
        if self.sl_steep.state:
            self.update_wave_height()
            self.sl_steep.state = False
        # check if the spectrum smoothing coefficient has been modified
        if self.sl_smooth.state:
            self.update_spectrum_smoothing()
            self.sl_smooth.state = False
        # check if the directional spreading function has been modified
        if self.sl_spread.state or self.sl_dir.state:
            self.update_directional_spreading()
            self.sl_spread.state, self.sl_dir.state = False, False
        # check if the high-frequency damping coefficient has been modified
        if self.sl_damp.state:
            self.update_high_frequency_damping()
            self.sl_damp.state = False
        # check if the high-pass filter coefficient has been modified
        if self.sl_filter.state:
            self.update_high_pass_filter_coefficient()
            self.sl_filter.state = False
        # check if the dispersion coefficient has been modified
        if self.sl_disp.state:
            self.update_dispersion()
            self.sl_disp.state = False
        # check if the maximal period has been modified
        if self.sl_Tmax.state:
            self.update_wavenumber_grid()
            self.update_wave_height()
            self.sl_Tmax.state = False

    def update_wavenumber_grid(self):
        '''
        Method that updates the relevant parameters of the wavenumber grid.
        '''
        # get the maximal wave period
        self.Tmax = self.sl_Tmax.x
        # get the longest wavelength
        self.L = 9.81*self.Tmax**2/(2*pi)
        # calculate the wavenumber vector
        self.k_vec = fft.fftshift(2*pi*fft.fftfreq(self.OS.Nk,
                                                   self.L/self.OS.Nk))
        # get the wavenumber discretization
        self.dk = self.k_vec[1]-self.k_vec[0]
        # calculate the wavenumber grids
        kx, ky = meshgrid(self.k_vec, self.k_vec, indexing='ij')
        if not hasattr(self, 'theta'):
            self.theta = arctan2(ky, kx)
        self.k = hypot(kx, ky)
        # get a wavenumber vector that is used to interpolate the sound
        # spectrum
        self.k_amp = linspace(self.k.min(), self.k.max(), self.OS.Nk//2)

    def update_colormap(self):
        '''
        Method that updates the colormap of the 3D plot to take into account
        modifications of the colormap type and its HSV corrections.
        '''
        cm_params = self.CCM.load(self.cb_cm.combobox.currentText())
        cm_params = self.CCM.apply_correction(cm_params,
                                              self.sl_hue.x,
                                              self.sl_sat.x,
                                              self.sl_val.x)
        self.cm = ColorMap(cm_params[:, 0], cm_params[:, 1:])

    def update_point_size(self):
        '''
        Methods that updates the size of the plotted points.
        '''
        self.point_size = self.sl_pts.x

    def update_cmap_limits(self):
        '''
        Methods that updates the limits of the colormap.
        '''
        # extreme colormap values for the mapped quantity
        self.vmin, self.vmax = self.sl_vmin.x, self.sl_vmax.x

    def update_points_display_style(self):
        '''
        Methods that updates the points display style of the 3D plot.
        '''
        self.pds = self.cb_pds.combobox.currentText()

    def update_mesh_display_style(self):
        '''
        Methods that updates the mesh display style of the 3D plot.
        '''
        self.mds = self.cb_mds.combobox.currentText()

    def update_mapped_quantity(self):
        '''
        Methods that updates the mapped quantity in the 3D plot.
        '''
        index = self.cb_mq.combobox.findText(self.cb_mq.combobox.currentText(),
                                             Qt.QtCore.Qt.MatchFixedString)
        self.mapped_quantity = self.cb_mq.kw_list[index]

    def update_plotting_style(self):
        '''
        Methods that updates the plotting style in the 3D plot.
        '''
        self.plotting_style = self.cb_pls.combobox.currentText()
        self.mod_style_state = True

    def update_wave_height(self):
        '''
        Methods that updates the significant wave height for the appropriate
        scaling of the wave spectrum.
        '''
        # make space_steep logarithmically vary between 0.001 and 0.1 for
        # sliders' value between 0 and 1
        if not hasattr(self, 'space_steep'):
            self.space_steep = geomspace(0.001, 0.1, 100)
        ind = int(self.sl_steep.x*len(self.space_steep))
        if ind == len(self.space_steep):
            ind -= 1
        # calculate the steepness
        eps = self.space_steep[ind]
        # calculate the significant wave height based on the longest wavelength
        self.Hs = eps*self.L

    def update_spectrum_smoothing(self):
        '''
        Methods that updates the spectrum smoothing coefficient for the wave
        spectrum calculation.
        '''
        # make val_smooth linearly vary between -10 and 0 for sliders' value
        # between 0 and 1
        val_smooth = 10*self.sl_smooth.x-10
        self.ramp = geomspace(10**val_smooth,
                              1+10**val_smooth,
                              self.n_frames)-10**val_smooth

    def update_directional_spreading(self):
        '''
        Methods that updates the directional spreading coefficient for the
        wave spectrum calculation.
        '''
        # make space_spread logarithmically vary between 1000 and 0.1 for
        # sliders' value between 0 and 1
        if not hasattr(self, 'space_spread'):
            self.space_spread = geomspace(1000., 0.1, 100)
        ind = int(self.sl_spread.x*len(self.space_spread))
        if ind == len(self.space_spread):
            ind -= 1
        # make val_dir linearly vary between pi and -pi for sliders' value
        # between 0 and 1
        val_dir = -2*pi*self.sl_dir.x+pi
        self.G = self.OS.directional_spreading_cos_no_scaling(
            self.theta, self.space_spread[ind], theta_dir=val_dir)

    def update_high_frequency_damping(self):
        '''
        Methods that updates the high-frequency damping coefficient for the
        wave spectrum calculation.
        '''
        # make val_damp linearly vary between 0 and -20 for sliders' value
        # between 0 and 1
        val_damp = -20*self.sl_damp.x
        self.damping_func = geomspace(1+10**val_damp,
                                      10**val_damp,
                                      self.OS.Nk//2)-10**val_damp

    def update_high_pass_filter_coefficient(self):
        '''
        Methods that updates the high-pass filter coefficient for the wave
        spectrum calculation.
        '''
        self.coef_filter = self.sl_filter.x

    def update_dispersion(self):
        '''
        Methods that updates the non-dimensional water depth for the
        calculation of the angular frequencies.
        '''
        # make space_disp logarithmically vary between 0.1 and 10 for sliders'
        # value between 0 and 1
        if not hasattr(self, 'space_disp'):
            self.space_disp = geomspace(0.01, 10., 100)
        ind = int(self.sl_disp.x*len(self.space_disp))
        if ind == len(self.space_disp):
            ind -= 1
        self.mu = self.space_disp[ind]

    def plot_data(self):
        '''
        Methods that updates the plotted data.
        '''
        # build the matrix of positions of the free surface particles
        if self.i_multiple:
            # if the domain is replicated multiple (2*2 = 4) times
            d = self.L+0.5*(self.OS.beta[-1, 1]-self.OS.beta[-1, 0])
            self.OS.x = vstack((hstack((self.OS.x, self.OS.x)),
                                hstack((self.OS.x+d, self.OS.x+d))))
            self.OS.y = vstack((hstack((self.OS.y, self.OS.y+d)),
                                hstack((self.OS.y, self.OS.y+d))))
            self.OS.z = vstack((hstack((self.OS.z, self.OS.z)),
                                hstack((self.OS.z, self.OS.z))))
        # update the colors
        if not (self.plotting_style == 'mesh' and self.mds == 'wireframe'):
            # if the plot is not the wireframe, update the mapped quantity
            if self.mapped_quantity == 'elevation':
                quantity = self.OS.z/self.Hs
            elif self.mapped_quantity == 'velocity x':
                self.OS.get_velocity('x')
                quantity = self.OS.U/(4*self.OS.U.std())
            elif self.mapped_quantity == 'velocity y':
                self.OS.get_velocity('y')
                quantity = self.OS.V/(4*self.OS.V.std())
            elif self.mapped_quantity == 'velocity z':
                self.OS.get_velocity('z')
                quantity = self.OS.W/(4*self.OS.W.std())
            elif self.mapped_quantity == 'absolute velocity':
                self.OS.get_velocity('all')
                quantity = (self.OS.vel-self.OS.vel.mean()) / \
                    (4*self.OS.vel.std())
            elif self.mapped_quantity == 'slope x':
                self.OS.get_slope('x')
                quantity = self.OS.sx
            elif self.mapped_quantity == 'slope y':
                self.OS.get_slope('y')
                quantity = self.OS.sy
            elif self.mapped_quantity == 'absolute slope':
                self.OS.get_slope('all')
                quantity = self.OS.slope
            else:
                # if the chosen quantity to map is not valid
                sys.exit('Please choose a valid quantity to map.')
            # adapt the value of the mapped quantity to the colormap limits
            if self.vmin != self.vmax:
                quantity = (quantity-self.vmin)/(self.vmax-self.vmin)
            else:
                # set the quantity to zero if the minimum and maximum limits of
                # the colormap are equal
                quantity *= 0.
            # map the quantity with the colormap
            self.color = self.cm.map(quantity)/255
        # update the visualization
        if self.plotting_style == 'mesh':
            # if the meah is plotted
            # get the mesh
            self.get_mesh()
            # update the mesh display style
            if self.mds == 'faces':
                drawEdges = False
                drawFaces = True
            elif self.mds == 'wireframe':
                drawEdges = True
                drawFaces = False
            else:
                # exit if mesh display style is not valid
                sys.exit('Please choose a valid mesh display style.')
            # plot the updated data and colormap
            if self.time_index == 0 or self.mod_style_state:
                # if it is the first time step or the plotting style has been
                # modified
                if self.mod_style_state:
                    # if the plotting style has been modified
                    # remove the previous plot
                    self.w.removeItem(self.sp)
                    # set the 'modified plotting style state' to False
                    self.mod_style_state = False
                # create the openGL widget containing the 3D plot
                self.sp = opengl.GLMeshItem(meshdata=self.mesh,
                                            edgeColor=(1., 1., 1., 1.),
                                            drawEdges=drawEdges,
                                            drawFaces=drawFaces,
                                            smooth=False,
                                            computeNormals=False)
                # add the plot to the openGL widget
                self.w.addItem(self.sp)
            else:
                # update the already existing 3D plot
                self.sp.setMeshData(meshdata=self.mesh,
                                    drawEdges=drawEdges,
                                    drawFaces=drawFaces)
        elif self.plotting_style == 'points':
            # if the points is plotted
            # construct the points' position matrix
            pos = stack((self.OS.x.ravel(),
                         self.OS.y.ravel(),
                         self.OS.z.ravel())).T
            # plot the updated data and colormap
            if self.time_index == 0 or self.mod_style_state:
                # if it is the first time step or the plotting style has been
                # modified
                if self.mod_style_state:
                    # if the plotting style has been modified
                    # remove the previous plot
                    self.w.removeItem(self.sp)
                    # set the 'modified plotting style state' to False
                    self.mod_style_state = False
                # create the openGL widget containing the 3D plot
                self.sp = opengl.GLScatterPlotItem(pos=pos/self.L,
                                                   color=self.color,
                                                   size=self.point_size,
                                                   glOptions=self.pds)
                # add the plot to the openGL widget
                self.w.addItem(self.sp)
            else:
                # update the already existing 3D plot
                self.sp.setData(pos=pos/self.L, color=self.color,
                                size=self.point_size)
                # set the points display style
                self.sp.setGLOptions(self.pds)
        else:
            # exit if plotting style is not valid
            sys.exit('Please choose a valid plotting style.')

    def set_sl_Tmax_state_to_true(self):
        '''
        Method that sets widget's state (boolean stating that value have been
        modified) to True. Following methods are similar but applied to other
        widgets.
        '''
        if not self.sl_Tmax.state:
            self.sl_Tmax.state = True

    def set_sl_steep_state_to_true(self):
        if not self.sl_steep.state:
            self.sl_steep.state = True

    def set_sl_smooth_state_to_true(self):
        if not self.sl_smooth.state:
            self.sl_smooth.state = True

    def set_sl_spread_state_to_true(self):
        if not self.sl_spread.state:
            self.sl_spread.state = True

    def set_sl_dir_state_to_true(self):
        if not self.sl_dir.state:
            self.sl_dir.state = True

    def set_sl_damp_state_to_true(self):
        if not self.sl_damp.state:
            self.sl_damp.state = True

    def set_sl_filter_state_to_true(self):
        if not self.sl_filter.state:
            self.sl_filter.state = True

    def set_sl_disp_state_to_true(self):
        if not self.sl_disp.state:
            self.sl_disp.state = True

    def set_sl_pts_state_to_true(self):
        if not self.sl_pts.state:
            self.sl_pts.state = True

    def set_sl_vmin_state_to_true(self):
        if not self.sl_vmin.state:
            self.sl_vmin.state = True

    def set_sl_vmax_state_to_true(self):
        if not self.sl_vmax.state:
            self.sl_vmax.state = True

    def set_sl_hue_state_to_true(self):
        if not self.sl_hue.state:
            self.sl_hue.state = True

    def set_sl_sat_state_to_true(self):
        if not self.sl_sat.state:
            self.sl_sat.state = True

    def set_sl_val_state_to_true(self):
        if not self.sl_val.state:
            self.sl_val.state = True

    def set_cb_pls_state_to_true(self):
        if not self.cb_pls.state:
            self.cb_pls.state = True

    def set_cb_cm_state_to_true(self):
        if not self.cb_cm.state:
            self.cb_cm.state = True

    def set_cb_mq_state_to_true(self):
        if not self.cb_mq.state:
            self.cb_mq.state = True

    def set_cb_mds_state_to_true(self):
        if not self.cb_mds.state:
            self.cb_mds.state = True

    def set_cb_pds_state_to_true(self):
        if not self.cb_pds.state:
            self.cb_pds.state = True

    def get_time(self, seconds):
        '''
        Method that splits a number of seconds into minutes, seconds and
        centiseconds, and gives the result as a string.

        Inputs
        ----------
        seconds :: float :: Number of seconds.

        Outputs
        ----------
        time :: string :: String indicating the number of minutes, seconds and
                          centiseconds in the input number of seconds.
        '''
        result = []
        # calculate the number of minutes, seconds and centiseconds
        for count in [60, 1, 0.01]:
            value = seconds//count
            # append the results
            if value:
                seconds -= value*count
                result.append('{:02.0f}'.format(value))
            else:
                value = 0.
                result.append('{:02.0f}'.format(value))
        # join the results, separated by ':'
        time = 'time is ' + ':'.join(result[:])
        return time

    def get_text_cam_params(self):
        '''
        Method that constructs the text displaying the camera parameters.
        '''
        self.cam_params = '𝐜𝐚𝐦𝐞𝐫𝐚 𝐩𝐚𝐫𝐚𝐦𝐞𝐭𝐞𝐫𝐬\n' + \
            'fov: {:.2f}° ― '.format(self.w.opts['fov']) + \
            'elevation: {:.2f}° ― '.format(self.w.opts['elevation']) + \
            'azimuth: {:.2f}°'.format(self.w.opts['azimuth']) + '\n' + \
            'center: ({:.2f}, {:.2f}, {:.2f}) ― '.format(
                self.w.opts['center'][0],
                self.w.opts['center'][1],
                self.w.opts['center'][2]) + \
            'distance: {:.2f}'.format(self.w.opts['distance'])

    def get_mesh(self):
        '''
        Method that constructs the matrices containing the (x,y,z) coordinates
        and corresponding colors of the mesh vertices.
        '''
        # construct the matrix of (x,y,z) coordinates of the mesh vertices
        # the size is (Nf, 3, 3), with Nf the number of triangular faces
        set1 = array([[self.OS.x[:-1, :-1].ravel(),
                       self.OS.y[:-1, :-1].ravel(),
                       self.OS.z[:-1, :-1].ravel()],
                      [self.OS.x[1:, :-1].ravel(),
                       self.OS.y[1:, :-1].ravel(),
                       self.OS.z[1:, :-1].ravel()],
                      [self.OS.x[:-1, 1:].ravel(),
                       self.OS.y[:-1, 1:].ravel(),
                       self.OS.z[:-1, 1:].ravel()]])
        set2 = array([[self.OS.x[1:, 1:].ravel(),
                       self.OS.y[1:, 1:].ravel(),
                       self.OS.z[1:, 1:].ravel()],
                      [self.OS.x[:-1, 1:].ravel(),
                       self.OS.y[:-1, 1:].ravel(),
                       self.OS.z[:-1, 1:].ravel()],
                      [self.OS.x[1:, :-1].ravel(),
                       self.OS.y[1:, :-1].ravel(),
                       self.OS.z[1:, :-1].ravel()]])
        vertexCoord = moveaxis(concatenate(
            (set1/self.L, set2/self.L), axis=-1), -1, 0)
        # construct the matrix of RGBalpha colors of the mesh vertices
        # the size is (Nf, 3, 4), with Nf the number of triangular faces
        if self.mds != 'wireframe':
            # if the plot is not the wireframe, update the vertex colors
            col1 = array([[self.color[:-1, :-1, 0].ravel(),
                           self.color[:-1, :-1, 1].ravel(),
                           self.color[:-1, :-1, 2].ravel(),
                           self.color[:-1, :-1, 3].ravel()],
                          [self.color[1:, :-1, 0].ravel(),
                           self.color[1:, :-1, 1].ravel(),
                           self.color[1:, :-1, 2].ravel(),
                           self.color[1:, :-1, 3].ravel()],
                          [self.color[:-1, 1:, 0].ravel(),
                           self.color[:-1, 1:, 1].ravel(),
                           self.color[:-1, 1:, 2].ravel(),
                           self.color[:-1, 1:, 3].ravel()]])
            col2 = array([[self.color[1:, 1:, 0].ravel(),
                           self.color[1:, 1:, 1].ravel(),
                           self.color[1:, 1:, 2].ravel(),
                           self.color[1:, 1:, 3].ravel()],
                          [self.color[:-1, 1:, 0].ravel(),
                           self.color[:-1, 1:, 1].ravel(),
                           self.color[:-1, 1:, 2].ravel(),
                           self.color[:-1, 1:, 3].ravel()],
                          [self.color[1:, :-1, 0].ravel(),
                           self.color[1:, :-1, 1].ravel(),
                           self.color[1:, :-1, 2].ravel(),
                           self.color[1:, :-1, 3].ravel()]])
            vertexColors = moveaxis(concatenate((col1, col2), axis=-1), -1, 0)
        else:
            vertexColors = zeros((vertexCoord.shape[0], 3, 4))
        # get the mesh according to the pyqtgraph.opengl formalism
        self.mesh = opengl.MeshData(vertexes=vertexCoord,
                                    vertexColors=vertexColors)

    def stream_callback_file(self, in_data, frame_count, time_info, status):
        '''
        Methods that is called by the opened pyaudio stream.

        Inputs
        ----------
        in_data :: 1darray :: Recorded data if input=True; else None.
        frame_count :: int :: Number of frames.
        time_info :: dict :: Dictionary with the following keys:
                             ``input_buffer_adc_time``, ``current_time`` and
                             ``output_buffer_dac_time``; see the PortAudio
                             documentation for their meanings.
        status :: PaCallbackFlags :: PortAutio Callback Flag; see the PortAudio
                                     documentation for more details.

        Outputs (formatted as a tuple)
        ----------
        out_data :: 1darray :: Ouput data. Not used here.
        flag :: PaCallbackReturnCodes :: Flag that states if the stream should
                                         continue, is complete, or should
                                         abort.
        '''
        if self.sound.qsize() == self.n_frames:
            self.sound.get()
        out_data = self.SQ.get()
        if out_data.ndim != 1:
            # put the average of the two channels if the sound input is stereo
            self.sound.put(out_data.mean(axis=1))
        else:
            # otherwise put the umodified mono sound data
            self.sound.put(out_data)
        if self.SQ.qsize() > 0:
            flag = pyaudio.paContinue
        else:
            flag = pyaudio.paComplete
        return (out_data, flag)

    def import_file_data(self, sound_queue):
        '''
        Method that imports the sound data (from the WAV file).
        '''
        self.SQ = sound_queue
